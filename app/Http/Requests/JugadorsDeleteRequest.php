<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class JugadorsDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // usuari quan la api_key es igual a Authorization, mostre primer o done error
        $user = User::where('api_key', $this->header('Authorization'))->firstOrFail();
        // si es usuari i es superadmin torna true sino false
        if (!is_null($user) && $user->superadmin) return true;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
