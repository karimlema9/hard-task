<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskDestroyRequest;
use App\Http\Requests\TaskIndexRequest;
use App\Http\Requests\TaskStoreRequest;
use App\Http\Requests\TaskUpdateRequest;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    public function index(TaskIndexRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
        return Task::all();
    }

    public function store(TaskStoreRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
    }

    public function destroy(TaskDestroyRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
    }

    public function update(TaskUpdateRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
    }
}
