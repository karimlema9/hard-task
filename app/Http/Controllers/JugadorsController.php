<?php

namespace App\Http\Controllers;

use App\Http\Requests\JugadorsDeleteRequest;
use App\Http\Requests\JugadorsIndexRequest;
use App\Http\Requests\JugadorsStoreRequest;
use App\Http\Requests\JugadorsUpdateRequest;
use App\Jugador;
use Illuminate\Http\Request;

class JugadorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Jugador[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(JugadorsIndexRequest $request)
    {
        return Jugador::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Jugador $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function destroy(JugadorsDeleteRequest $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JugadorsStoreRequest $jugador)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function show(Jugador $jugador)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function edit(Jugador $jugador)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function update(JugadorsUpdateRequest $request)
    {
        //
    }


}
