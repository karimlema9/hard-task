<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\ComentsUpdateRequest;
use App\Http\Requests\CommentDestroyRequest;
use App\Http\Requests\CommentIndexRequest;
use App\Http\Requests\CommentStoreRequest;
use App\User;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function index(CommentIndexRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
        return Comment::all();
    }

    public function store(CommentStoreRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
    }

    public function destroy(CommentDestroyRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
    }

    public function update(ComentsUpdateRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
    }
}
