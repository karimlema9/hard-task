<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test void
     */
    public function can_superadmin_list_comments()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>true]);
        $response=$this->json('GET','/api/v1/comments',[],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
    }

    /**
     * A basic feature test example.
     *
     * @test void
     */
    public function can_regular_user_list_comments()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>false]);
        $response=$this->json('GET','/api/v1/comments',[],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
        //$response->assertStatus(403);
    }

    /**
     * @test
     */
    public function can_superadmin_create_comments()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>true]);
        $response=$this->json('POST','/api/v1/comments',[
            'id' => '1',
            'title' => 'Funciona correctament',
            'description' => 'El programam funiona pero...'
        ],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function can_regular_user_create_comments()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>false]);
        $response=$this->json('POST','/api/v1/comments',[
            'id' => '1',
            'title' => 'Funciona correctament',
            'description' => 'El programam funiona pero...'
        ],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function can_superadmin_delete_comments()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>true]);
        $response=$this->json('DELETE','/api/v1/comments/{id}',[],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function can_regular_user_delete_comments()
    {
        //$this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>false]);
        $response=$this->json('DELETE','/api/v1/comments/{id}',[],['Authorization'=>$user->api_key]);
        $response->assertStatus(403);
    }


    /**
     * @test
     */
    public function can_superadmin_update_comments()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>true]);
        $response=$this->json('PUT','/api/v1/comments/{id}',[
            'id' => '2',
            'title' => 'Bla bala',
            'description' => 'Bla bla bla...'
        ],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function can_regular_user_update_comments()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>true]);
        $response=$this->json('PUT','/api/v1/comments/{id}',[
            'id' => '2',
            'title' => 'Bla bala',
            'description' => 'Bla bla bla...'
        ],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
    }
}
