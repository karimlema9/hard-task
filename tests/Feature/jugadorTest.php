<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class jugadorTest extends TestCase
{
//    /**
//     * A basic feature test example.
//     *
//     * @return void
//     * @test
//     */
//    public function users_acces_to_url()
//    {
//        // creem usuaris
//        $user = factory(User::class)->create([
//            'superadmin' => false
//        ]);
//        // accedim a la url adecuada
//        $response = $this->json('GET','/api/v1/jugadors');
//        // esperem 200
//        $response->assertSuccessful();
//
//    }

    /**
     * @test
     */
    public function superadmin_can_list_jugadors()
    {
        // creem usuari admin
        $user = factory(User::class)->create([
            'superadmin' => true
        ]);
        // demanem la ruta                   -afegir dades de l'usuari _   *Header de la get
        $response = $this->json('GET', '/api/v1/jugadors',[],['Authorization'=>$user->api_key]);
        //$response);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function regular_user_cannot_list_jugadors()
    {
        // creem usuari admin
        $user = factory(User::class)->create([
            'superadmin' => false
        ]);
        // demanem la ruta                   -afegir dades de l'usuari _   *Header de la get
        $response = $this->json('GET', '/api/v1/jugadors',[],['Authorization'=>$user->api_key]);
        //dd($response);
        //$response->assertSuccessful();
        //Comprovem que ens mostra el codi 403 de que no te permissos per accedir
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function superadmin_can_create_jugadors()
    {
        //$this->withoutExceptionHandling();
        // creem usuari admin
        $user = factory(User::class)->create([
            'superadmin' => true
        ]);
        // demanem la ruta                   -afegir dades de l'usuari _   *Header de la get
        $response = $this->json('POST', '/api/v1/jugadors',[
            'name' => 'karim',
            'age' => '21'
        ],['Authorization'=>$user->api_key]);
        //dd($response);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function regular_user_cannot_create_jugadors()
    {
        // creem usuari admin
        $user = factory(User::class)->create([
            'superadmin' => false
        ]);
        // demanem la ruta                   -afegir dades de l'usuari _   *Header de la get
        $response = $this->json('POST', '/api/v1/jugadors',[
            'name' => 'karim',
            'age' => '21'
        ],['Authorization'=>$user->api_key]);
        //dd($response);
//        $response->assertSuccessful();
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function superadmin_can_delete_jugadors()
    {
//        $this->withoutExceptionHandling();
        // creem usuari admin
        $user = factory(User::class)->create([
            'superadmin' => true
        ]);
        // demanem la ruta                   -afegir dades de l'usuari _   *Header de la get
        $response = $this->json('DELETE', '/api/v1/jugadors/{id}',[],['Authorization'=>$user->api_key]);
        //dd($response);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function regular_user_cannot_delete_jugadors()
    {
        //$this->withoutExceptionHandling();
        // creem usuari admin
        $user = factory(User::class)->create([
            'superadmin' => false
        ]);
        // demanem la ruta                   -afegir dades de l'usuari _   *Header de la get
        $response = $this->json('DELETE', '/api/v1/jugadors/{id}',[],['Authorization'=>$user->api_key]);
        //dd($response);
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function superadmin_can_update_jugadors()
    {
//        $this->withoutExceptionHandling();
        // creem usuari admin
        $user = factory(User::class)->create([
            'superadmin' => true
        ]);
        // demanem la ruta                   -afegir dades de l'usuari _   *Header de la get
        $response = $this->json('PUT', '/api/v1/jugadors/{id}',[
            'name' => 'karim',
            'age' => '22'
        ],['Authorization'=>$user->api_key]);
        //dd($response);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function regular_user_cannot_update_jugadors()
    {
        //$this->withoutExceptionHandling();
        // creem usuari admin
        $user = factory(User::class)->create([
            'superadmin' => false
        ]);
        // demanem la ruta                   -afegir dades de l'usuari _   *Header de la get
        $response = $this->json('PUT', '/api/v1/jugadors/{id}',[
            'name' => 'karim',
            'age' => '22'
        ],['Authorization'=>$user->api_key]);
        //dd($response);
        $response->assertStatus(403);
    }
}
