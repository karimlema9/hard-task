<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class tasktest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test void
     */
    public function can_superadmin_list_task()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>true]);
        $response=$this->json('GET','/api/v1/tasks',[],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
    }

    /**
     * A basic feature test example.
     *
     * @test void
     */
    public function can_regular_user_list_task()
    {
        //$this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>false]);
        $response=$this->json('GET','/api/v1/tasks',[],['Authorization'=>$user->api_key]);
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function can_superadmin_create_task()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>true]);
        $response=$this->json('POST','/api/v1/tasks',[
            'name' => 'Compra pa',
            'user_id' => '1',
            'description' => 'Anar al supermercat i compa pa',
            'finish' => false
        ],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function can_regular_user_create_task()
    {
        //$this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>false]);
        $response=$this->json('POST','/api/v1/tasks',[
            'name' => 'Compra pa',
            'user_id' => '1',
            'description' => 'Anar al supermercat i compa pa',
            'finish' => false
        ],['Authorization'=>$user->api_key]);
        $response->assertStatus(403);
    }

    /**
 * @test
 */
    public function can_superadmin_delete_task()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>true]);
        $response=$this->json('DELETE','/api/v1/tasks/{id}',[],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function can_regular_user_delete_task()
    {
        //$this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>false]);
        $response=$this->json('DELETE','/api/v1/tasks/{id}',[],['Authorization'=>$user->api_key]);
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function can_superadmin_update_task()
    {
        $this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>true]);
        $response=$this->json('PUT','/api/v1/tasks/{id}',[
            'name' => 'Compra llet',
            'user_id' => '2',
            'description' => 'Anar al supermercat i compa llet',
            'finish' => true
        ],['Authorization'=>$user->api_key]);
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function can_regular_user_update_task()
    {
        //$this->withoutExceptionHandling();
        $user=factory(User::class)->create(['superadmin'=>false]);
        $response=$this->json('PUT','/api/v1/tasks/{id}',[
            'name' => 'Compra llet',
            'user_id' => '2',
            'description' => 'Anar al supermercat i compa llet',
            'finish' => true
        ],['Authorization'=>$user->api_key]);
        $response->assertStatus(403);
    }
}
